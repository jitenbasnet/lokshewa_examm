package com.loksewaquiz.helper;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import com.loksewaquiz.NavigationActivity;
import com.loksewaquiz.activity.RegisterActivity;

import java.util.HashMap;

public class SessionManager {


    SharedPreferences pref;
    Editor editor;

    Context _context;

    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "LoksewaExam";

    // Fabcebok Shared Preferences Keys
    private static final String FACEBOOK_LOGIN = "IsFbLoggedIn";
    // Manual Loggedin Shared Preferences Keys
    private static final String MANUAL_LOGIN = "IsManualLoggedIn";
    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";

    // User name (make variable public to access from outside)
    public static final String KEY_NAME = "name";

    // Email address (make variable public to access from outside)
    public static final String KEY_EMAIL = "email";

    // Constructor
    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }


    /**
     * Create FBlogin session
     */
    public void createFBLoginSession(String name, String email) {
        // Storing login value as TRUE
        editor.putBoolean(FACEBOOK_LOGIN, true);

        // commit changes
        editor.commit();
    }

    /**
     * Create FBlogin session
     */
    public void createManualLoginSession(String name, String email) {
        // Storing login value as TRUE
        editor.putBoolean(MANUAL_LOGIN, true);

        // Storing name in pref
        editor.putString(KEY_NAME, name);

        // Storing email in pref
        editor.putString(KEY_EMAIL, email);

        // commit changes
        editor.commit();
    }

    /**
     * Check login method wil check user login status
     * If false it will redirect user to login page
     * Else won't do anything
     */
    public void checkLogin() {
        // Check login status
        if (!this.isLoggedIn()) {
            // user is not logged in redirect him to Login Activity

            Intent i = new Intent(_context, RegisterActivity.class);

            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            _context.startActivity(i);
        }

    }


    /**
     * Get stored session data
     */
    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        // user name
        user.put(KEY_NAME, pref.getString(KEY_NAME, null));

        // user email id
        user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, null));

        // return user
        return user;
    }

    /**
     * Clear session details
     */
    public void logoutUser() {
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();

        // After logout redirect user to Loing Activity
        Intent i = new Intent(_context,NavigationActivity.class);
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Staring Login Activity
        _context.startActivity(i);
    }

    /**
     * Quick check for login
     **/
// Get Login State
    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }

    // Get FACBEBOOK Login State
    public boolean isFbLoggedIn() {
        return pref.getBoolean(FACEBOOK_LOGIN, false);
    }

    // Get MANUAL Login State
    public boolean isManualLoggedIn() {
        return pref.getBoolean(MANUAL_LOGIN, false);
    }

    /**
     * public void setLogin(boolean isLoggedIn) {
     * <p>
     * editor.putBoolean(IS_LOGIN, isLoggedIn);
     * <p>
     * // commit changes
     * editor.commit();
     * <p>
     * Log.e("value of isLoggedIn", String.valueOf((isLoggedIn)));
     * }
     **/
    public void setFBLogin(boolean isFBLoggedIn) {

        editor.putBoolean(FACEBOOK_LOGIN, isFBLoggedIn);

        // commit changes
        editor.commit();

        Log.e("value of isFBLoggedIn", String.valueOf((isFBLoggedIn)));
    }

    public void setManualLogin(boolean isManualLoggedIn) {

        editor.putBoolean(MANUAL_LOGIN, isManualLoggedIn);

        // commit changes
        editor.commit();

        Log.e("value of isManualLogin", String.valueOf((isManualLoggedIn)));
    }


}