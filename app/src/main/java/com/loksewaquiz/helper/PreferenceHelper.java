package com.loksewaquiz.helper;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Digition on 6/10/2017.
 */

public class PreferenceHelper{

    private SharedPreferences sharedPreference;
    public PreferenceHelper(Context context)
    {
        sharedPreference = context.getSharedPreferences("DB", Context.MODE_APPEND);

    }

    public void put(String tag, String name)
    {
        SharedPreferences.Editor editor = sharedPreference.edit();

        editor.putString(tag, name);
        editor.apply();

    }
    public String get(String tag, String defaultString)
    {
        return sharedPreference.getString(tag, defaultString);
    }

}
