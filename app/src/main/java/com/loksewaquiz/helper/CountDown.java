package com.loksewaquiz.helper;

import android.widget.TextView;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by Digition on 6/23/2017.
 */

public class CountDown {


    public void setTimer(final TextView tv, long time) {
        new android.os.CountDownTimer(time, 1000) {

            public void onTick(long millisUntilFinished) {
                String text = String.format(Locale.getDefault(), "%02d:%02d",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) % 60,
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) % 60);
                tv.setText(text);
            }

            public void onFinish() {
                tv.setText("done!");
            }
        }.start();
    }


}
