package com.loksewaquiz.helper;

import android.os.AsyncTask;
import android.util.Log;

import com.loksewaquiz.Model_classes.FormObjects;

import java.io.IOException;
import java.net.URL;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by kumar on 6/22/2017.
 */

public class PostRequest  extends AsyncTask<Object,Integer,String> {


    private String responseBody;
    @Override
    protected String doInBackground(Object... objects){

        FormBody.Builder formBuilder = new FormBody.Builder();

        for(int i= 1; i < objects.length; i++) {
            formBuilder.add(((FormObjects)objects[i]).getName(),((FormObjects)objects[i]).getValue());
        }
        RequestBody requestBody = formBuilder.build();
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url((URL)objects[0]).post(requestBody).build();

        try {
            Response response = client.newCall(request).execute();
            if(response.isSuccessful()) {
                responseBody = response.body().string();
                Log.e("Response Body", responseBody);
                return "OK" ;
            }


            if(response.code() == 401) return "Unauthorized";

            Log.e("Error", response.code() + response.body().string());
            return "NOT OK";

        }catch (IOException e) {
            return e.toString();
        }
    }

    public String getResponseBody()
    {
        return responseBody;
    }

}
