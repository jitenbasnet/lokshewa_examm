package com.loksewaquiz;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loksewaquiz.adapter.AayogAdapter;
import com.loksewaquiz.dto.AayogDTO;
import com.loksewaquiz.util.PrefUtil;
import com.loksewaquiz.widget.DividerItemDecoration;
import com.loksewaquiz.widget.ProgressWheel;

import java.lang.reflect.Type;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;

public class LoksewaAayog extends AppCompatActivity {
    private InterstitialAd interstitial;

    private final String Tag = LoksewaAayog.class.getSimpleName();
    AayogAdapter aayogAdapter;
    List<String> stringList = new ArrayList<String>();
    RecyclerView recyclerView;
    Toolbar toolbar;
    List<AayogDTO> aayo = new ArrayList<AayogDTO>();
    String[] strings;
    private TextView titleText;
    private String actionBarTitle="";
    private AdView mAdView1,mAdView2;
    private AdRequest adRequest;
    Toolbar tb;
    ProgressWheel progressWheel;
    private boolean isInMainPage = true;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loksewa_aayog);
        AdView adView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .setRequestAgent("android_studio:ad_template").build();
        adView.loadAd(adRequest);
        tb = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(tb);
        tb.setTitle("लोक सेवा आयोग सुचनाहरु");
        progressWheel = (ProgressWheel)findViewById(R.id.progress_wheel);
        setupRecyclerView();
        setUpMainPage();

    }

    public static String hashString(String message)
    {
        try
        {
            MessageDigest md = MessageDigest.getInstance("MD5"); //or "SHA-1"
            md.update(message.getBytes());
            BigInteger hash = new BigInteger(1, md.digest());
            String result = hash.toString(16);
            while (result.length() < 32)
            { //40 for SHA-1
                result = "0" + result;
            }
            return result;
        }catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }


    private void setUpMainPage()
    {

        PrefUtil.pusString("inMainPage", "Yes", this);
        strings = getResources().getStringArray(R.array.loksewa_aayog);
        for(String temp: strings)
        {
            String[] splits = temp.split(",");
            stringList.add(splits[0]);
            Log.e("Strings", temp + "sp" + splits[0]);
        }
        aayogAdapter.notifyDataSetChanged();

        progressWheel.setVisibility(View.GONE);
    }

    public void setUpSecondaryPage(String title)
    {
        progressWheel.setVisibility(View.VISIBLE);
        this.isInMainPage = false;
        tb.setTitle(title);
        PrefUtil.pusString("inMainPage", "NO", this);
        String action = "";
        String hash = "";
        for(String i: strings)
        {

            String[] splits = i.split(",");
            //AppLog.showLog(splits[0], title);
            if(splits[0].equals(title)){
                action = splits[1];
                hash = hashString(splits[2]);
                break;
            }
        }





        RequestQueue mRequestQueue;

        Cache cache = new DiskBasedCache(getCacheDir(), 1); // 1 byte cap
        Network network = new BasicNetwork(new HurlStack());
        mRequestQueue = new RequestQueue(cache, network);
        mRequestQueue.start();

        // Toast.makeText(applicationContext, "Here", Toast.LENGTH_SHORT).show();

        String url =  "http://app.loksewaguide.com/index.php?per_page=500&page=1&action=" + action + "&key=" + hash;

        final StringRequest stringRequest = new StringRequest(Request.Method.GET,url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{

                            Type listType = new TypeToken<List<AayogDTO>>(){}.getType();

                            List<AayogDTO> aayogs = new Gson().fromJson(response, listType);

                            stringList.clear();
                            progressWheel.setVisibility(View.GONE)  ;


                            for(AayogDTO a: aayogs)
                            {
                                aayo.add(a);
                                stringList.add(a.getTitle());
                            }
                            aayogAdapter.notifyDataSetChanged();

                        }catch (Exception e){

                            Toast.makeText(LoksewaAayog.this,"Something Went Wrong Please Try Again", Toast.LENGTH_SHORT).show();

                            setUpMainPage();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(LoksewaAayog.this, error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
        mRequestQueue.add(stringRequest);

    }

    private void setupRecyclerView()
    {
        recyclerView = (RecyclerView)findViewById(R.id.recyclerview);
        aayogAdapter = new AayogAdapter(stringList, this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this));
        recyclerView.setAdapter(aayogAdapter);

    }

    public void goDownload(int position)
    {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(aayo.get(position).getUrl()));
            startActivity(intent);
        }catch (IndexOutOfBoundsException e){

        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if(!isInMainPage )
        {
            Intent intent = new Intent(this, LoksewaAayog.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }
        else{
            super.onBackPressed();
        }
    }


}
