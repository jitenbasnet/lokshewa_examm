package com.loksewaquiz;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.widget.ProfilePictureView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.loksewaquiz.Model_classes.Exam_Title_Lists;
import com.loksewaquiz.activity.About_APP;
import com.loksewaquiz.activity.LoginActivity;
import com.loksewaquiz.adapter.PackageAdapter;
import com.loksewaquiz.app.AppController;
import com.loksewaquiz.dto.PackageDTO;
import com.loksewaquiz.fetch_exam_title_lists_fromMYSQL.Exam_Lists_Downloader;
import com.loksewaquiz.helper.CountDown;
import com.loksewaquiz.helper.Loksewa_SQLiteHandler;
import com.loksewaquiz.helper.SessionManager;
import com.loksewaquiz.questions_lists_UI.ExamAdapter;


import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;


public class NavigationActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    final static String urlAddress = "http://loksewaexam.loksewaguide.com/loksewaquiz/api/user/packageFetch.php";
    private List<PackageDTO> packageDTOs = new ArrayList<>();
    private TextView profile_Name;
    private TextView profile_Email;
    private ProfilePictureView profile_Image;
    private Loksewa_SQLiteHandler db;
    private SessionManager session;
    private int drawableId;
    private String imageName = "alert";
    String profileFullName = "Loksewa";
    String emails = "loksewaexam@gmail.com";
    String profileImageLink = "";
    String checkActivity = "check";
    ProfilePictureView profileView;
    TextView profileName;
    TextView profileEmail;
    TextView ids;
    TextView firstname;
    String profile_FullName;
    String email;
    String profile_ImageLink;
    String id;
    String first_name;
    String mytoken = "";


    int nayab_marks_fm,
            section_marks_fm, kharidar_marks_fm, political_marks_fm,
            nayab_marks_pm,
            section_marks_pm, kharidar_marks_pm, political_marks_pm;


    int id_2, id_3, id_4, id_5, nayab_time, section_time, kharidar_time, political_time;

    String nayab_subba, section_officer, kharidar, political_science;

    Context c;
    Exam_Title_Lists st[] = new Exam_Title_Lists[4];
    ArrayList<Exam_Title_Lists> main_exam_list;
    String currentVersion;
    String latestVersion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());

        setContentView(R.layout.activity_navigation);

        VersionChecker versionChecker = new VersionChecker();
        try {
            latestVersion = versionChecker.execute().get();

        }catch (InterruptedException | ExecutionException ex){

        }

        try{
            currentVersion = getPackageManager().getPackageInfo(getPackageName(),0).versionName;



        }catch (PackageManager.NameNotFoundException nm){

        }
        if(Float.valueOf(currentVersion)<Float.valueOf(latestVersion)) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(NavigationActivity.this);
            dialog.setCancelable(false);
            dialog.setTitle("New Version of App available");
            dialog.setMessage("Do you want to install the fresh Update?");
            dialog.setPositiveButton("Update", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    //Action for "Delete".
                    Intent playStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.loksewaquiz"));
                    startActivity(playStoreIntent);
                }
            })
                    .setNegativeButton("Cancel ", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //Action for "Cancel".
                        }
                    });

            final AlertDialog alert = dialog.create();
            alert.show();
        }else{

        }

        AdView adView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .setRequestAgent("android_studio:ad_template").build();
        adView.loadAd(adRequest);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        AccessToken token = AccessToken.getCurrentAccessToken();

        setupRecyclerView();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        //Navigation View
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        assert navigationView != null;
        View headerLayout = navigationView.getHeaderView(0);
        navigationView.setNavigationItemSelectedListener(this);
        // session manager
        session = new SessionManager(getApplicationContext());
        if (session.isFbLoggedIn() || session.isLoggedIn() || session.isManualLoggedIn()) {
            MenuItem menuItemLogin = navigationView.getMenu().getItem(4);
            menuItemLogin.setVisible(false);

        } else {
            MenuItem menuItemLogout = navigationView.getMenu().getItem(3);
            menuItemLogout.setVisible(false);

        }

        profileView = (ProfilePictureView) headerLayout.findViewById(R.id.profile_image);
        profileName = (TextView) headerLayout.findViewById(R.id.profile_name);
        //profileEmail = (TextView) headerLayout.findViewById(R.id.profile_user_id);


        if (session.isManualLoggedIn()) {
            // get user data from session
            HashMap<String, String> user = session.getUserDetails();
            // name
            String name = user.get(SessionManager.KEY_NAME);

            // email
           // String email = user.get(SessionManager.KEY_EMAIL);

            // displaying user data
            if (name != null) {
                profileName.setText("welcome" + " " + name);
            } else {
                profileName.setText("welcome");

            }
           // profileEmail.setText((email));


        } else if (session.isFbLoggedIn()) {
            try {

                String profile_FullName = returnValueFromBundles(FacebookActivity.PROFILE_NAME);
                String email = returnValueFromBundles(FacebookActivity.PROFILE_EMAIL);
                String profile_ImageLink = returnValueFromBundles(FacebookActivity.PROFILE_IMAGE_URL);
                String id = returnValueFromBundles(FacebookActivity.PROFILE_ID);
                String last_name = returnValueFromBundles(FacebookActivity.PROFILE_LAST_NAME);
                Log.i("ProfileFullName", profileFullName);
                //shared preference for id
                SharedPreferences value_from_fb = this.getSharedPreferences("FB", Context.MODE_PRIVATE);
                SharedPreferences.Editor fb_editor = value_from_fb.edit();
                fb_editor.putString("fb_fullName", profile_FullName);
                fb_editor.putString("fb_email", email);
                fb_editor.putString("fb_imageLink", profile_ImageLink);
                fb_editor.putString("fb_id", id);
                fb_editor.putString("fb_first_name", first_name);
                fb_editor.commit();
                //shared prefernce to get exam titles
                SharedPreferences get_value_fb = this.getSharedPreferences("FB", this.MODE_PRIVATE);
                String profile_fullname = get_value_fb.getString("fb_fullName", "missing");
                String profile_email = get_value_fb.getString("fb_email", "missing");
                String profile_imageLink = get_value_fb.getString("fb_imageLink", "missing");
                String profile_id = get_value_fb.getString("fb_id", "missing");
                String profile_first_name = get_value_fb.getString("fb_first_name", "missing");

                profileName.setText(profile_fullname + " " + last_name);
                //profileEmail.setText(profile_email);
                profileView.setProfileId(profile_imageLink);
                ids.setText(profile_id);
                firstname.setText(profile_first_name);


            } catch (NullPointerException ex) {

            }
        } else {

        }


        // SqLite database handler
        db = new Loksewa_SQLiteHandler(getApplicationContext());

        //shared prefernce to get Access_token from loginActivity
        if (session.isManualLoggedIn()) {

            SharedPreferences get_access_token = this.getSharedPreferences("ACCESS_TOKEN_KEY", this.MODE_PRIVATE);
            this.mytoken = get_access_token.getString("login_access_token", "4aa5a9f6495159df0120ed9cab5a8622");
        } else if (session.isFbLoggedIn()) {

            SharedPreferences fb_get_access_token = NavigationActivity.this.getSharedPreferences("FB_ACCESS_TOKEN", this.MODE_PRIVATE);
            this.mytoken = fb_get_access_token.getString("facebook_access_token", "4aa5a9f6495159df0120ed9cab5a8622");

        }
        if (mytoken != null) {




/*clicking this exam title it takes the parameter exam_of and sends it
        to the MainActivity for fetching the questions lists*/
//            nayab.setOnClickListener(new View.OnClickListener() {
//                @Override
//
//                public void onClick(View view) {
//                    if (session.isFbLoggedIn() || session.isManualLoggedIn()) {
//                        setupAccessToken();
//                        Toast.makeText(getApplicationContext(), "You are logged in", Toast.LENGTH_LONG).show();
//                        Intent intent = new Intent(NavigationActivity.this, MainActivity.class);
//                        intent.putExtra("EXAM_ID", id_two);
//                        intent.putExtra("ACCESS_TOKEN", mytoken);
//                        intent.putExtra("NAYAB_TIME", nayab_times);
//                        intent.putExtra("POLITICAL_MARK", nayab_mark_fm);
//                        intent.putExtra("POLITICAL_MARK_PM", nayab_mark_pm);
//                        startActivity(intent);
//
//                    } else {
//                        session.checkLogin();
//
//                    }
//
//
//                }
//            });
//            section.setOnClickListener(new View.OnClickListener() {
//                @Override
//
//                public void onClick(View view) {
//                    if (session.isFbLoggedIn() || session.isManualLoggedIn()) {
//                        setupAccessToken();
//                        Toast.makeText(getApplicationContext(), "You are logged in", Toast.LENGTH_LONG).show();
//                        Intent intent = new Intent(NavigationActivity.this, MainActivity.class);
//                        intent.putExtra("EXAM_ID", id_three);
//                        intent.putExtra("SECTION_TIME", section_times);
//                        intent.putExtra("SECTION_MARK", section_mark_fm);
//                        intent.putExtra("POLITICAL_MARK_PM", section_mark_pm);
//                        intent.putExtra("ACCESS_TOKEN", mytoken);
//                        startActivity(intent);
//
//
//                    } else {
//                        session.checkLogin();
//
//
//                    }
//
//
//                }
//
//            });
//
//            kharidar_btn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    if (session.isFbLoggedIn() || session.isManualLoggedIn()) {
//                        setupAccessToken();
//                        Toast.makeText(getApplicationContext(), "You are logged in", Toast.LENGTH_LONG).show();
//                        Intent intent = new Intent(NavigationActivity.this, MainActivity.class);
//                        intent.putExtra("KHARIDAR_TIME", kharidar_times);
//                        intent.putExtra("POLITICAL_MARK", kharidar_mark_fm);
//                        intent.putExtra("POLITICAL_MARK_PM", political_mark_pm);
//
//                        intent.putExtra("EXAM_ID", id_four);
//                        intent.putExtra("ACCESS_TOKEN", mytoken);
//                        startActivity(intent);
//
//
//                    } else {
//
//                        session.checkLogin();
//                    }
//
//
//                }
//            });
//            political.setOnClickListener(new View.OnClickListener() {
//                @Override
//
//                public void onClick(View view) {
//                    if (session.isFbLoggedIn() || session.isManualLoggedIn()) {
//                        setupAccessToken();
//                        Toast.makeText(getApplicationContext(), "You are logged in", Toast.LENGTH_LONG).show();
//                        Intent intent = new Intent(NavigationActivity.this, MainActivity.class);
//                        intent.putExtra("POLITICAL_TIME", political_times);
//                        intent.putExtra("EXAM_ID", id_five);
//                        intent.putExtra("POLITICAL_MARK", political_mark_fm);
//                        intent.putExtra("POLITICAL_MARK_PM", political_mark_pm);
//                        intent.putExtra("ACCESS_TOKEN", mytoken);
//                        startActivity(intent);
//                    } else {
//                        session.checkLogin();
//
//                    }
//                }
//            });
//        } else {
//            Toast.makeText(NavigationActivity.this, "Please Login", Toast.LENGTH_SHORT).show();
//        }
//
//        loksewa_news.setOnClickListener(new View.OnClickListener() {
//            @Override
//
//            public void onClick(View view) {
//                Intent intent = new Intent(NavigationActivity.this, LoksewaAayog.class);
//                startActivity(intent);
//            }
//        });


        }
    }

    public void setupRecyclerView()
    {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.packageRecyclerView);
        //categoryAdapter = new ExamAdapter(this, questionArrayList);
        PackageAdapter packageAdapter = new PackageAdapter(this, packageDTOs);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        //recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(packageAdapter);

        List<PackageDTO> temp = AppController.getInstance().getPackageList();
        for(PackageDTO packageDTO: temp)
        {
            Log.e("TEMP", packageDTO.getPackage_name());

            packageDTOs.add(packageDTO);
        }


        packageAdapter.notifyDataSetChanged();
    }


    private void setupAccessToken()
    {
        if(session.isManualLoggedIn())
        {
            SharedPreferences get_access_token = getSharedPreferences("ACCESS_TOKEN_KEY", MODE_PRIVATE);
            mytoken = get_access_token.getString("login_access_token","4aa5a9f6495159df0120ed9cab5a8622");
        }else if(session.isFbLoggedIn()){
            SharedPreferences fb_get_access_token = getSharedPreferences("FACEBOOK_ACCESS_TOKEN", MODE_PRIVATE);
            mytoken = fb_get_access_token.getString("facebook_access_token","4aa5a9f6495159df0120ed9cab5a8622");

        }
    }

    @Override
    public void onBackPressed() {

        moveTaskToBack(true);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
       /* if (id == R.id.action_settings) {
            return true;
        } */
        if (id == R.id.action_login) {
            Intent i = new Intent(this, LoginActivity.class);
            startActivity(i);
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_news) {
            Intent intent = new Intent(NavigationActivity.this,LoksewaAayog.class);
            startActivity(intent);
            // Handle the camera action
        } else if (id == R.id.nav_facebook) {
            LoginManager.getInstance().logOut();
            session.setFBLogin(false);
            session.setManualLogin(false);
            Log.e("Logout of valuefb", String.valueOf(session.isFbLoggedIn()));
            Log.e("Logout of valueManu", String.valueOf(session.isManualLoggedIn()));
            Toast.makeText(this, "YOU JUST LOGGED OUT!!", Toast.LENGTH_SHORT).show();

            Intent logoutIntent = new Intent(NavigationActivity.this, FacebookActivity.class);
            startActivity(logoutIntent);


        } else if (id == R.id.nav_info) {
            Intent intent = new Intent(NavigationActivity.this, About_APP.class);
            startActivity(intent);


        } else if (id == R.id.nav_info) {
            Intent intent = new Intent(NavigationActivity.this, About_APP.class);
            startActivity(intent);



        } else if (id == R.id.nav_logout) {
            // Logout button click event
            db.deleteUsers();
            // Launching the login activity
            session.logoutUser();
            session.setFBLogin(false);
            session.setManualLogin(false);
            getBaseContext().getSharedPreferences("ACCESS_TOKEN", 0).edit().clear().apply();
            getBaseContext().getSharedPreferences("FACEBOOK_ACCESS_TOKEN", 0).edit().clear().apply();
            Log.e("Logout of valuefb", String.valueOf(session.isFbLoggedIn()));
            Log.e("Logout of valueManu", String.valueOf(session.isManualLoggedIn()));
            Toast.makeText(this, "YOU JUST LOGGED OUT!!", Toast.LENGTH_SHORT).show();

        } else if (id == R.id.nav_login) {

            Intent intent = new Intent(NavigationActivity.this, LoginActivity.class);
            startActivity(intent);
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;


    }

    /**
     * Logging out the user. Will set isLoggedIn flag to false in shared
     * preferences Clears the user data from sqlite users table
     */

    public void NavigationActivitys(Context c, ArrayList<Exam_Title_Lists> main_exam_lists) {
        this.c = c;
        main_exam_list = main_exam_lists;
        this.st[0] = main_exam_lists.get(0);
        this.st[1] = main_exam_lists.get(1);
        this.st[2] = main_exam_lists.get(2);
        this.st[3] = main_exam_lists.get(3);
        //exam titles
        this.nayab_subba = st[0].getPackage_name();
        this.section_officer = st[1].getPackage_name();
        this.kharidar = st[2].getPackage_name();
        this.political_science = st[3].getPackage_name();
        //exam id
        this.id_2 = st[0].getId();
        this.id_3 = st[1].getId();
        this.id_4 = st[2].getId();
        this.id_5 = st[3].getId();

        //time
        this.nayab_time = st[0].getTime();
        this.section_time = st[1].getTime();
        this.kharidar_time = st[2].getTime();
        this.political_time = st[3].getTime();
        //fm
        this.nayab_marks_fm = st[0].getFull_mark();
        this.section_marks_fm = st[1].getFull_mark();
        this.kharidar_marks_fm = st[2].getFull_mark();
        this.political_marks_fm = st[3].getFull_mark();
        //pm
        this.nayab_marks_pm = st[0].getPass_mark();
        this.nayab_marks_pm = st[1].getPass_mark();
        this.nayab_marks_pm = st[2].getPass_mark();
        this.nayab_marks_pm = st[3].getPass_mark();

        //shared preferences for title
        SharedPreferences titles = c.getSharedPreferences("titles", Context.MODE_PRIVATE);
        SharedPreferences.Editor title_editor = titles.edit();
        title_editor.putString("NAYAB_SUBBA", nayab_subba);
        title_editor.putString("SECTION_OFFICER", section_officer);
        title_editor.putString("KHARIDAR", kharidar);
        title_editor.putString("POLITICAL_SCIENCE", political_science);
        title_editor.commit();

        //shared preference for id
        SharedPreferences id = c.getSharedPreferences("id", Context.MODE_PRIVATE);
        SharedPreferences.Editor id_editor = id.edit();
        id_editor.putInt("EXAM_ID_TWO", id_2);
        id_editor.putInt("EXAM_ID_THREE", id_3);
        id_editor.putInt("EXAM_ID_FOUR", id_4);
        id_editor.putInt("EXAM_ID_FIVE", id_5);
        id_editor.commit();
        //shared preference for time
        SharedPreferences time = c.getSharedPreferences("time", Context.MODE_PRIVATE);
        SharedPreferences.Editor time_editor = time.edit();
        time_editor.putInt("NAYAB_TIME", nayab_time);
        time_editor.putInt("SECTION_TIME", section_time);
        time_editor.putInt("KHARIDAR_TIME", kharidar_time);
        time_editor.putInt("POLITICAL_TIME", political_time);
        time_editor.commit();
        //shared preference for fm
        SharedPreferences marks_fm = c.getSharedPreferences("marks_fm", Context.MODE_PRIVATE);
        SharedPreferences.Editor fm_marks_editor = marks_fm.edit();
        fm_marks_editor.putInt("NAYAB_MARKS", nayab_marks_fm);
        fm_marks_editor.putInt("SECTION_MARKS", section_marks_fm);
        fm_marks_editor.putInt("KHARIDAR_MARKS", kharidar_marks_fm);
        fm_marks_editor.putInt("POLITICAL_MARKS", political_marks_fm);
        fm_marks_editor.commit();
        //shared preference for pm
        SharedPreferences marks_pm = c.getSharedPreferences("marks_pm", Context.MODE_PRIVATE);
        SharedPreferences.Editor marks_pm_editor = marks_pm.edit();
        marks_pm_editor.putInt("NAYAB_MARKS_PM", nayab_marks_pm);
        marks_pm_editor.putInt("SECTION_MARKS_PM", section_marks_pm);
        marks_pm_editor.putInt("KHARIDAR_MARKS_PM", kharidar_marks_pm);
        marks_pm_editor.putInt("POLITICAL_MARKS_PM", political_marks_pm);
        marks_pm_editor.commit();


    }

    private String returnValueFromBundles(String key) {
        Bundle inBundle = getIntent().getExtras();
        String returnedValue = String.valueOf(inBundle.get(key));
        return returnedValue;
    }



}
