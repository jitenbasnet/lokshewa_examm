package com.loksewaquiz.Model_classes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Digition on 6/14/2017.
 */

public class QuestionsResponse {

    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("title")
    @Expose
    public String title;


    public boolean hasImage() {
        return this.image != null;
    }


    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getImage() {
        return image;
    }

    public List<AnsOption> getAnsOption() {
        return ansOption;
    }

    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("ans_option")
    @Expose
    public List<AnsOption> ansOption = null;


}
