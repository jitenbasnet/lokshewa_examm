package com.loksewaquiz.Model_classes;

/**
 * Created by Digition on 6/8/2017.
 */

public class Exam_Title_Lists {
    String package_name;
    int time;
    int full_mark;
    int pass_mark;
    int number_of_question;
    int mark_of_each_question;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    int id;

    public String getPackage_name() {
        return package_name;
    }

    public void setPackage_name(String package_name) {
        this.package_name = package_name;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public int getFull_mark() {
        return full_mark;
    }

    public void setFull_mark(int full_mark) {
        this.full_mark = full_mark;
    }

    public int getPass_mark() {
        return pass_mark;
    }

    public void setPass_mark(int pass_mark) {
        this.pass_mark = pass_mark;
    }

    public int getNumber_of_question() {
        return number_of_question;
    }

    public void setNumber_of_question(int number_of_question) {
        this.number_of_question = number_of_question;
    }

    public int getMark_of_each_question() {
        return mark_of_each_question;
    }

    public void setMark_of_each_question(int mark_of_each_question) {
        this.mark_of_each_question = mark_of_each_question;
    }


}