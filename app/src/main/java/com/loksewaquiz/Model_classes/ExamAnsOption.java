package com.loksewaquiz.Model_classes;

/**
 * Created by Digition on 6/13/2017.
 */

public class ExamAnsOption {
    String option_option;
    int option_id,correct;

    public String getOption_option() {
        return option_option;
    }

    public void setOption(String option_option) {
        this.option_option = option_option;
    }

    public int getOption_id() {
        return option_id;
    }

    public void setOption_id(int option_id) {
        this.option_id = option_id;
    }

    public int getCorrect() {
        return correct;
    }

    public void setCorrect(int correct) {
        this.correct = correct;
    }
}
