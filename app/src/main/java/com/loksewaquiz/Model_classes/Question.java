package com.loksewaquiz.Model_classes;

/**
 * Created by kumar on 6/21/2017.
 */

public class Question {


    private QuestionsResponse questionsResponse;
    private String selectedAnswer;

    public QuestionsResponse getQuestionsResponse() {
        return questionsResponse;
    }

    public void setQuestionsResponse(QuestionsResponse questionsResponse) {
        this.questionsResponse = questionsResponse;
    }

    public String getSelectedAnswer() {

        if (selectedAnswer == null)
            return "Unanswered";
        return selectedAnswer;
    }

    public boolean isAnswered() {
        return selectedAnswer != null;
    }

    public void setSelectedAnswer(String selectedAnswer) {
        this.selectedAnswer = selectedAnswer;
    }


    public boolean isCorrect() {
        if (selectedAnswer == null || selectedAnswer.isEmpty()) return false;
        for (AnsOption option : questionsResponse.ansOption) {
            if (option.correct == 1) {
                return selectedAnswer.equals(option.getOptionOption());
            }
        }
        return false;
    }


    public AnsOption getCorrect() {
        for (AnsOption option : questionsResponse.ansOption) {
            if (option.correct == 1) {
                return option;
            }
        }
        return null;
    }


}
