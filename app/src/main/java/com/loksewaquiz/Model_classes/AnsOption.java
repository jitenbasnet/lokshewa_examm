package com.loksewaquiz.Model_classes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Digition on 6/14/2017.
 */
public class AnsOption {

    @SerializedName("option_id")
    @Expose
    public Integer optionId;
    @SerializedName("option_option")
    @Expose
    public String optionOption;
    @SerializedName("correct")
    @Expose
    public Integer correct;

    public Integer getOptionId() {
        return optionId;
    }

    public String getOptionOption() {
        return optionOption;
    }

    public Integer getCorrect() {
        return correct;
    }
}