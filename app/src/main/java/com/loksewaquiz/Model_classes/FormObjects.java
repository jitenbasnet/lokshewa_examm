package com.loksewaquiz.Model_classes;

/**
 * Created by kumar on 6/22/2017.
 */

public class FormObjects {
    private String name;
    private String value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
