package com.loksewaquiz.splashActiviy;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.FacebookSdk;
import com.loksewaquiz.FacebookActivity;
import com.loksewaquiz.Fetch_Package_Task;
import com.loksewaquiz.NavigationActivity;
import com.loksewaquiz.R;
import com.loksewaquiz.app.NetworkCheckClass;
import com.loksewaquiz.helper.SessionManager;

import java.util.Timer;
import java.util.TimerTask;


public class Launcher_SplashActivity extends AppCompatActivity {
    AccessTokenTracker accessTokenTracker;
    SessionManager session;
    Context c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(getApplicationContext());


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher__splash);
        session = new SessionManager(Launcher_SplashActivity.this);
        c = this;


        Timer t = new Timer();
        boolean checkConnection = new NetworkCheckClass().checkConnection(this);
        if (checkConnection) {

            try{
                Fetch_Package_Task fetch_package_task = new Fetch_Package_Task(this);
                fetch_package_task.execute().get();
                Intent intent = new Intent(this, NavigationActivity.class);
                startActivity(intent);
                finish();
            }catch (Exception e){

            }


        } else {
            // Create an Alert Dialog
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            // Set the Alert Dialog Message
            builder.setMessage("" + "Internet Connection Required.")
                    .setCancelable(false)
                    .setPositiveButton("Retry",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    // Restart the Activity
                                    Intent intent = getIntent();
                                    finish();
                                    startActivity(intent);
                                }
                            });
            AlertDialog alert = builder.create();
            alert.show();
            final Button positiveButton = alert.getButton(AlertDialog.BUTTON_POSITIVE);
            LinearLayout.LayoutParams positiveButtonLL = (LinearLayout.LayoutParams) positiveButton.getLayoutParams();
            positiveButtonLL.width = ViewGroup.LayoutParams.MATCH_PARENT;
            positiveButton.setLayoutParams(positiveButtonLL);
        }
    }



}

