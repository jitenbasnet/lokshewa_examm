package com.loksewaquiz.splashActiviy;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.FacebookSdk;
import com.loksewaquiz.FacebookActivity;
import com.loksewaquiz.NavigationActivity;
import com.loksewaquiz.R;
import com.loksewaquiz.helper.SessionManager;

/**
 * Created by Digition on 6/24/2017.
 */

public class Splash_Activity extends AppCompatActivity {
    AccessTokenTracker accessTokenTracker;
    SessionManager session;
    Context c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(getApplicationContext());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken newAccessToken) {
                updateWithToken(newAccessToken);
            }
        };

        updateWithToken(AccessToken.getCurrentAccessToken());
        session = new SessionManager(Splash_Activity.this);
    }


    public void updateWithToken(AccessToken currentAccessToken) {

        if (currentAccessToken != null) {
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    Intent i = new Intent(Splash_Activity.this, NavigationActivity.class);
                    startActivity(i);
                    finish();
                }
            },1000);
        } else {
            if (session.isFbLoggedIn()) {
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        Intent i = new Intent(Splash_Activity.this, FacebookActivity.class);
                        startActivity(i);
                        finish();
                    }
                },1000);
            } else if (session.isManualLoggedIn()) {
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        Intent i = new Intent(Splash_Activity.this, NavigationActivity.class);
                        startActivity(i);
                        finish();
                    }
                },1000);


            }
        }


    }
}
