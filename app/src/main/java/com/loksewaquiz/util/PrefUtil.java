package com.loksewaquiz.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

/**
 * Created by lokex on 12/24/14.
 * Utility class to handle shared pref operations
 */
public class PrefUtil {

    private static final String PREF_USER_INFO = "user_info";
    private static final String PREF_NAME_GENERAL = "prefs";
    private static final String KEY_TOKEN = "token";
    private static final String KEY_USER_ID = "user_id";
    private static final String KEY_USER_EMAIL = "email";
    private static final String KEY_USER_NAME = "name";
    private static final String FIRST_LAUNCH = "first_launch";
    private static final String PREF_NAME_GCM = "gcm";
    private static final String PREF_NAME_INFORMATION = "information";
    private static final String PREF_NAME_SERVICE = "service";
    boolean isViewed;


//    SecurePreferences securePreferences = new SecurePreferences(getCurrentContext());
//    SharedPreferences.Editor editor = securePreferences.edit();
//    editor.putString("Token","ABCDEFGH").commit();
//
//
//    AppLog.showLog(TAG,"Token::"+securePreferences.getString("Token",null));




    public static void setFirstLaunch(Context context, boolean value) {
        SharedPreferences hasLearned = context.getSharedPreferences(PREF_NAME_GENERAL, 0);
        SharedPreferences.Editor hasRunEditor = hasLearned.edit();
        hasRunEditor.putBoolean(FIRST_LAUNCH, value).commit();

    }
    public static SharedPreferences getGcmPreference(Context context) {
        return context.getSharedPreferences(PREF_NAME_GCM, 0);
    }

    public static boolean isFirstLaunch(Context context) {
        SharedPreferences hasLearned = context.getSharedPreferences(PREF_NAME_GENERAL, 0);
        return hasLearned.getBoolean(FIRST_LAUNCH, true);

    }
    public static void storeUserEmail(Context context, String email) {
        SharedPreferences preferences = context.getSharedPreferences(PREF_NAME_GENERAL, 0);
        SharedPreferences.Editor hasRunEditor = preferences.edit();

        hasRunEditor.putString(KEY_USER_EMAIL, email).commit();

    }


    public static void storeUserID(Context context,int userId){
        SharedPreferences securePreferences = context.getSharedPreferences("abc",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = securePreferences.edit();


        editor.putInt(KEY_USER_ID,userId).commit();

    }

    public static void storeInformation(Context context, String information) {
        SharedPreferences preferences = context.getSharedPreferences(PREF_NAME_GENERAL, 0);
        SharedPreferences.Editor hasRunEditor = preferences.edit();
        hasRunEditor.putString(PREF_NAME_INFORMATION, information).commit();

    }


    public static String getInformation(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(PREF_NAME_GENERAL, 0);
        return preferences.getString(PREF_NAME_INFORMATION, "");
    }
    public static void storeService(Context context, String service) {
        SharedPreferences preferences = context.getSharedPreferences(PREF_NAME_GENERAL, 0);
        SharedPreferences.Editor hasRunEditor = preferences.edit();
        hasRunEditor.putString(PREF_NAME_SERVICE, service).commit();

    }


    public static String getService(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(PREF_NAME_GENERAL, 0);
        return preferences.getString(PREF_NAME_SERVICE, "");
    }

    public static String getUserEmail(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(PREF_NAME_GENERAL, 0);
        return preferences.getString(KEY_USER_EMAIL, "");
    }

    public static void storeUserName(Context context, String email) {
        SharedPreferences preferences = context.getSharedPreferences(PREF_NAME_GENERAL, 0);
        SharedPreferences.Editor hasRunEditor = preferences.edit();

        hasRunEditor.putString(KEY_USER_NAME, email).commit();

    }


    public static String getUserName(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(PREF_NAME_GENERAL, 0);
        return preferences.getString(KEY_USER_NAME, "");
    }

    public static int getUserID(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("abc",Context.MODE_PRIVATE);

        return preferences.getInt(KEY_USER_ID, 0);
    }


    public  static boolean isNewVersionAvailable(Context context)
    {
        SharedPreferences preferences = context.getSharedPreferences("abc",Context.MODE_PRIVATE);

        return preferences.getBoolean("UPDATE", false);
    }

    public  static  void setUpdateStatus(boolean status, Context context)
    {
        SharedPreferences securePreferences = context.getSharedPreferences("abc",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = securePreferences.edit();

        editor.putBoolean("UPDATE",status).commit();


    }



    public static void pusString(String key,String value,Context context)
    {
        SharedPreferences securePreferences = context.getSharedPreferences("abc",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = securePreferences.edit();

        editor.putString(key,value).commit();

    }

    public static String getString(String key,String defaultValue,Context context)
    {
        SharedPreferences preferences = context.getSharedPreferences("abc",Context.MODE_PRIVATE);

        return preferences.getString(key,defaultValue);
    }
}
