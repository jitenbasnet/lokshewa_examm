package com.loksewaquiz.util;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.loksewaquiz.R;


/**
 * this is custom font class, first declare the same name class in attrs file(example given bellow) and you can extends custom fonts even  buttom  edit text  or wherever you want to implement
 * and keep you all font in assets/fonts directory  or you can make simple fonts directory in assets directory.
 * <p/>
 * how to declare in attrs file eg.
 * <declare-styleable name="CustomTextView">
 * <attr name="fontName" format="string" />
 * </declare-styleable>
 */
public class CustomTextView extends android.support.v7.widget.AppCompatTextView {

    public CustomTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);

    }

    public CustomTextView(Context context) {
        super(context);
        init(null);
    }

    private void init(AttributeSet attrs) {
        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.CustomTextView);
            String fontName = a.getString(R.styleable.CustomTextView_fontName);
            if (fontName != null) {
                Typeface myTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/" + fontName);
                setTypeface(myTypeface);
            }
            a.recycle();
        }
    }

}
