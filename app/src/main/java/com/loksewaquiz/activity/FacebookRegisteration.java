package com.loksewaquiz.activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.loksewaquiz.app.AppConfig;
import com.loksewaquiz.app.AppController;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by Digition on 6/24/2017.
 */
public class FacebookRegisteration {


    public void registerUser(final String firstname, final String lastname,
                             final String email, final String username, final String password, final String fbid) {
        // Tag used to cancel the request
        String tag_string_req = "req_register";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_REGISTER, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e("FACEBOOK_REGISTER", "Register Response: " + response);
                try {
                    Log.e("Response String: ", response);
                    JSONObject jObj = new JSONObject(response);
                    int user_id = jObj.getInt("user_id");
                    String fb_access_token = jObj.getString("access_token");
                    if (fb_access_token != null) {
                        Toast.makeText(getApplicationContext(), "Login Successful", Toast.LENGTH_LONG).show();
                        //Toast.makeText(getApplicationContext(), user_id + access_token , Toast.LENGTH_LONG).show();
                        //shared preference for access-token obtained from Facebook
                        SharedPreferences fb_access_token_preference = getApplicationContext().getSharedPreferences("FACEBOOK_ACCESS_TOKEN", Context.MODE_PRIVATE);
                        SharedPreferences.Editor fb_access_token_editor = fb_access_token_preference.edit();
                        fb_access_token_editor.putString("facebook_access_token", fb_access_token);
                        fb_access_token_editor.commit();
                    }

                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //  Toast.makeText(getApplicationContext(),
                //error.networkResponse.statusCode + "Status ", Toast.LENGTH_LONG).show();
                Toast.makeText(getApplicationContext(),
                        "Error Posting Facebook Details", Toast.LENGTH_LONG).show();


            }
        }) {


            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<>();
                params.put("firstname", firstname);
                params.put("lastname", lastname);
                params.put("email", email);
                params.put("username", username);
                params.put("password", password);
                params.put("fbid", fbid);

                return params;
            }


        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
}
