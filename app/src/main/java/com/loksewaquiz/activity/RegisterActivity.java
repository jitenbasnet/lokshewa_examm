package com.loksewaquiz.activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.loksewaquiz.NavigationActivity;
import com.loksewaquiz.R;
import com.loksewaquiz.app.AppConfig;
import com.loksewaquiz.app.AppController;
import com.loksewaquiz.helper.Loksewa_SQLiteHandler;
import com.loksewaquiz.helper.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class RegisterActivity extends AppCompatActivity {
    private static final String TAG = RegisterActivity.class.getSimpleName();
    private Button btnRegister;
    private Button btnLinkToLogin;
    private EditText inputFirstNAME;
    private EditText inputLastNAME;
    private EditText inputEmail;
    private EditText inputPassword;
    private EditText inputUserName;
    private ProgressDialog pDialog;
    private Loksewa_SQLiteHandler db;
    private SessionManager session;
    String username;
    Context c;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ActionBar bar = getSupportActionBar();
        // bar.setDisplayHomeAsUpEnabled(true);


        inputFirstNAME = (EditText) findViewById(R.id.fname);
        inputLastNAME = (EditText) findViewById(R.id.Lname);
        inputEmail = (EditText) findViewById(R.id.email);
        inputPassword = (EditText) findViewById(R.id.password);
        inputUserName = (EditText) findViewById(R.id.username);
        btnRegister = (Button) findViewById(R.id.btnRegister);
        btnLinkToLogin = (Button) findViewById(R.id.btnLinkToLoginScreen);


        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        c=RegisterActivity.this;

        // Session manager
        session = new SessionManager(getApplicationContext());

        // SQLite database handler
        db = new Loksewa_SQLiteHandler(getApplicationContext());

        // Check if user is already logged in or not
        if (session.isLoggedIn()) {
            // User is already logged in. Take him to main activity
            Intent intent = new Intent(RegisterActivity.this,
                    NavigationActivity.class);
            startActivity(intent);
            finish();
        }


        // Register Button Click event
        btnRegister.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String firstname = inputFirstNAME.getText().toString();
                String lastname = inputLastNAME.getText().toString();
                String email = inputEmail.getText().toString();
                String password = inputPassword.getText().toString().trim();
                RegisterActivity.this.username = inputUserName.getText().toString();


                if (!firstname.isEmpty() && !lastname.isEmpty() && !email.isEmpty() && password!= null && !username.isEmpty() ) {
                    registerUser(firstname, lastname, email, password, username);
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Please enter your details!", Toast.LENGTH_LONG)
                            .show();
                }
            }
        });

        // Link to Login Screen


        btnLinkToLogin.setOnClickListener(new View.OnClickListener() {



            public void onClick(View view) {

                Intent i = new Intent(RegisterActivity.this, LoginActivity.class);
                i.putExtra("username",username);
                startActivity(i);
                finish();



            }
        });


    }

    /**
     * Function to store user in MySQL database will post params(tag, name,
     * email, password) to register url
     */
    public void registerUser(final String firstname, final String lastname,
                             final String email, final String password,final String username) {
        // Tag used to cancel the request
        String tag_string_req = "req_register";

        pDialog.setMessage("Registering ...");

        showDialog();



        StringRequest strReq = new StringRequest(Method.POST,
                AppConfig.URL_REGISTER, new Response.Listener<String>()
        {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "Register Response: " + response);
                Toast.makeText(getApplicationContext(), "User successfully registered. Try login now!", Toast.LENGTH_LONG).show();

                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    // Launch login activity
                    // Launch login activity
                    Intent intent = new Intent(
                            RegisterActivity.this,
                            LoginActivity.class);

                    intent.putExtra("USERNAME",username);
                    startActivity(intent);
                    finish();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getApplicationContext(),
                //  error.networkResponse.statusCode + "Status ", Toast.LENGTH_LONG).show();
                Toast.makeText(getApplicationContext(),
                        "Something went Wrong..", Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {


            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<>();
                params.put("firstname", firstname);
                params.put("lastname", lastname);
                params.put("email", email);
                params.put("password", password);
                params.put("username", username);


                return params;
            }


        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}