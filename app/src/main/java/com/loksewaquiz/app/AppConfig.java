package com.loksewaquiz.app;

/**
 * Created by Digition on 5/17/2016.
 */
    public class AppConfig {
        // Server user login url
        public static String URL_LOGIN = "http://loksewaexam.loksewaguide.com/loksewaquiz/api/user/verifyUser.php";

        // Server user register url
        public static String URL_REGISTER = "http://loksewaexam.loksewaguide.com/loksewaquiz/api/user/signup.php";
        public static String URL_SUBMIT_RESULTS = "http://loksewaexam.loksewaguide.com/loksewaquiz/api/submit_results.php";

    //url to fetch question
    public static String URL_GETQUESTION = "http://loksewaexam.loksewaguide.com/loksewaquiz/api/getQuestions.php";




}
