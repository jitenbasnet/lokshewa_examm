package com.loksewaquiz.app;

import android.app.Application;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.loksewaquiz.Model_classes.Question;
import com.loksewaquiz.dto.PackageDTO;
import com.loksewaquiz.helper.PreferenceHelper;

import java.util.List;

public class AppController extends Application {
    private List<Question> questionList;

    private List<PackageDTO> packageList;
    public static final String TAG = AppController.class.getSimpleName();

    private RequestQueue mRequestQueue;

    private static AppController mInstance;
    public PreferenceHelper preferenceHelper;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        preferenceHelper = new PreferenceHelper(this);
    }


    public void setQuestion(List<Question> question)
    {
        this.questionList = question;
    }

    public List<Question> getQuestionList()
    {
        return this.questionList;
    }

    public static synchronized AppController getInstance()
    {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }


    public void setPackageList(List<PackageDTO> packageList)
    {


        this.packageList = packageList;

        //Log.e("Package List", this.packageList.get(0).getPackage_name());
    }

    public List<PackageDTO> getPackageList(){
        return this.packageList;
    }

}