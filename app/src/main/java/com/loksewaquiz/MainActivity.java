package com.loksewaquiz;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loksewaquiz.Model_classes.FormObjects;
import com.loksewaquiz.Model_classes.Question;
import com.loksewaquiz.Model_classes.QuestionsResponse;
import com.loksewaquiz.app.AppConfig;
import com.loksewaquiz.app.AppController;
import com.loksewaquiz.dto.PackageDTO;
import com.loksewaquiz.helper.CountDown;
import com.loksewaquiz.helper.PostRequest;
import com.loksewaquiz.helper.SessionManager;
import com.loksewaquiz.questions_lists_UI.ExamAdapter;
import com.loksewaquiz.splashActiviy.Launcher_SplashActivity;
import com.loksewaquiz.widget.ProgressWheel;

import java.lang.reflect.Type;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;


/*** Created by Digition on 6/2/2017.
 */
public class MainActivity extends AppCompatActivity {

    private Context context = this;
    private static final String TAG = MainActivity.class.getSimpleName();
    private ProgressDialog pDialog;
    String Response = "";
    RecyclerView recyclerView;
    ExamAdapter categoryAdapter;
    long durationSeconds = 2700;
    TextView countdown;
    private int position;
    boolean doubleBackToExitPressedOnce = false;
    CountDownTimer countDownTimer;
    boolean timehasstarted = false;
    private ArrayList<Question> questionArrayList = new ArrayList<>();
    private ArrayList<Question> unansweredList = new ArrayList<>();
    Gson gson;
    TextView fm_pm;
    SessionManager session;
    String access_token;
    ProgressWheel progressWheel;
    private InterstitialAd interstitial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.fetch_questions_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final Button button = (Button) findViewById(R.id.seeResults);
        progressWheel = (ProgressWheel)findViewById(R.id.progress_wheel);
        progressWheel.setVisibility(View.GONE);
        interstitial = new InterstitialAd(MainActivity.this);
        AdView adView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .setRequestAgent("android_studio:ad_template").build();
        adView.loadAd(adRequest);
        setupRecyclerView();
        fm_pm = (TextView) findViewById(R.id.marks);
        session = new SessionManager(this);

        if (session.isManualLoggedIn()) {

            SharedPreferences get_access_token = getSharedPreferences("ACCESS_TOKEN_KEY", this.MODE_PRIVATE);
            this.access_token = get_access_token.getString("login_access_token", "4aa5a9f6495159df0120ed9cab5a8622");
        } else if (session.isFbLoggedIn()) {

            SharedPreferences fb_get_access_token = getSharedPreferences("FACEBOOK_ACCESS_TOKEN", this.MODE_PRIVATE);
            this.access_token = fb_get_access_token.getString("facebook_access_token", "4aa5a9f6495159df0120ed9cab5a8622");

        }

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        //Receive
        Log.e(TAG, "The value OF access_token is" + access_token);

        // String user_id = i.getExtras().getString("USER_ID_KEY");
        Intent myIntent = this.getIntent();
        Bundle b = myIntent.getExtras();
//        final int exam_of = b.getInt("EXAM_ID");
//        final int nayab_times = b.getInt("NAYAB_TIME");
//        final int section_times = b.getInt("SECTION_TIME");
//        final int kharidar_times = b.getInt("KHARIDAR_TIME");
//        final int political_times = b.getInt("POLITICAL_TIME");
        position = b.getInt("position");
       //Log.e(TAG, "VALUE OF nayab,section,kharidar,political is " + (nayab_times+ section_times+kharidar_times+political_times));
        Log.e(TAG, "VALUE OF exam_of is " + (position));
        //i.getExtras().getString("EXAM_ID_TWO") ;
        //Toast.makeText(this, access_token, Toast.LENGTH_LONG).show();
        final PackageDTO packageDTO = AppController.getInstance().getPackageList().get(position);
        checkLogin(access_token, packageDTO.getId());
        countdown = (TextView) findViewById(R.id.timer);

        fm_pm.setText("FM:"+packageDTO.getFull_mark() + "\nPm:"+packageDTO.getPass_mark());

        new CountDownTimer(packageDTO.getTime() * 1000,1000){
            @Override
            public void onTick(long millisUntilFinished) {
                countdown.setText(""+String.format("%d:%d",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.
                                        toMinutes(millisUntilFinished))));
            }

            @Override
            public void onFinish() {
                button.post(new Runnable() {
                    @Override
                    public void run() {
                        button.performClick();
                    }
                });
            }
        }.start();

//        if (exam_of == 2) {
//            fm_pm.setText("Fm:"+100+"\nPm:"+40);
//            new CountDownTimer(nayab_times*1000,1000) {
//
//                public void onTick(long millisUntilFinished) {
//                    countdown.setText(""+String.format("%d:%d",
//                            TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
//                            TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
//                                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.
//                                            toMinutes(millisUntilFinished))));
//                }
//
//                public void onFinish() {
//                    button.post(new Runnable() {
//                        @Override
//                        public void run() {
//                            button.performClick();
//                        }
//                    });
//                }
//            }.start();
//
//
//        }  if (exam_of == 3) {
//
//            new CountDownTimer(3600000,1000) {
//
//                public void onTick(long millisUntilFinished) {
//                    countdown.setText(""+String.format("%d:%d",
//                            TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
//                            TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
//                                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.
//                                            toMinutes(millisUntilFinished))));
//                }
//
//                public void onFinish() {
//                    button.post(new Runnable() {
//                        @Override
//                        public void run() {
//                            button.performClick();
//                        }
//                    });
//
//
//                }
//            }.start();
//            fm_pm.setText("Fm:"+80+"\nPm:"+32);
//        }
//        if (exam_of == 4) {
//            new CountDownTimer(kharidar_times*1000, 1000) {
//
//                public void onTick(long millisUntilFinished) {
//                    countdown.setText(""+String.format("%d:%d",
//                            TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
//                            TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
//                                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.
//                                            toMinutes(millisUntilFinished))));
//                }
//
//                public void onFinish() {
//                    button.post(new Runnable() {
//                        @Override
//                        public void run() {
//                            button.performClick();
//                        }
//                    });
//                }
//            }.start();
//            fm_pm.setText("Fm:"+100+"\nPm:"+40);
//        }  if (exam_of == 5) {
//            fm_pm.setText("Fm:"+50+"\nPm:"+20);
//            new CountDownTimer(political_times*1000,1000) {
//
//                public void onTick(long millisUntilFinished) {
//                    countdown.setText(""+String.format("%d:%d",
//                            TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
//                            TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
//                                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.
//                                            toMinutes(millisUntilFinished))));
//                }
//
//                public void onFinish() {
//                    button.post(new Runnable() {
//                        @Override
//                        public void run() {
//                            button.performClick();
//                        }
//                    });
//                }
//            }.start();
//        }


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayInterestialAd();
                progressWheel.setVisibility(View.VISIBLE);
                int correct = 0, unanswred = 0;
                for (Question q : questionArrayList) {
                    if (!q.isAnswered()) unanswred++;
                    if (q.isCorrect()) correct++;
                }
                submitResult(access_token, packageDTO.getId(), unanswred, correct);
                AppController.getInstance().setQuestion(questionArrayList);
                Intent intent = new Intent(context, ResultDetails.class);
                startActivity(intent);
                progressWheel.setVisibility(View.GONE);
                finish();
            }
        });


    }
    public void displayInterestialAd() {
        interstitial = new InterstitialAd(getApplicationContext());
        interstitial.setAdUnitId(getString(R.string.int_ad));
        AdRequest adRequest = new AdRequest.Builder().build();
        interstitial.loadAd(adRequest);
        interstitial.setAdListener(new AdListener() {
            public void onAdLoaded() {
                if (interstitial.isLoaded()) {
                    interstitial.show();
                }
            }
        });

    }



    private void submitResult(final String access_token, final int exam_of, final int unanswered, final int correct) {
        FormObjects[] formObjects = new FormObjects[4];
        formObjects[0] = new FormObjects();
        formObjects[0].setName("access_token");
        formObjects[0].setValue(access_token);
        formObjects[1] = new FormObjects();
        formObjects[1].setName("exam_of");
        formObjects[1].setValue(String.valueOf(exam_of));
        formObjects[2] = new FormObjects();
        formObjects[2].setName("unanswered");
        formObjects[2].setValue(String.valueOf(unanswered));
        formObjects[3] = new FormObjects();
        formObjects[3].setName("correct");
        formObjects[3].setValue(String.valueOf(correct));

        try {

            String uri = AppConfig.URL_SUBMIT_RESULTS;
            URL url = new URL(uri);

            String postRequest = new PostRequest().execute(url, formObjects[0], formObjects[1], formObjects[2], formObjects[3]).get();
            //Toast.makeText(MainActivity.this, postRequest, Toast.LENGTH_SHORT).show();
            if (postRequest == "OK") {
                //Toast.makeText(MainActivity.this, "Account Created", Toast.LENGTH_SHORT).show();
                Log.e("Add", "add");
            } else Log.e("Error", "Erro");
            //Toast.makeText(MainActivity.this, "Please try other username or email", Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            //Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            Log.e("Error occured", e.getMessage());
        }
    }

    private void checkLogin(final String access_token, final int exam_of) {

        Log.e("Log", access_token + "\t" + exam_of);

        pDialog.setMessage("Fetching questions ...");
        showDialog();

        StringRequest sr = new StringRequest(Request.Method.POST, AppConfig.URL_GETQUESTION, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response);
                gson = new Gson();
                Type listType = new TypeToken<List<QuestionsResponse>>() {
                }.getType();
                List<QuestionsResponse> responses = gson.fromJson(response, listType);
                hideDialog();
                questionArrayList.clear();
                for (QuestionsResponse response1 : responses) {
                    Question question = new Question();
                    question.setQuestionsResponse(response1);

                    questionArrayList.add(question);
                    categoryAdapter.notifyDataSetChanged();

                }


            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error.Response", error.toString());
                Toast.makeText(MainActivity.this, "Something went Wrong!", Toast.LENGTH_SHORT).show();
                pDialog.dismiss();
                Intent i = new Intent(MainActivity.this, Launcher_SplashActivity.class);
                startActivity(i);
                finish();


            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("access_token", access_token);
                params.put("exam_of", String.valueOf(exam_of));
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

        };


        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(sr);
    }


    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    public void setupRecyclerView() {
        recyclerView = (RecyclerView) findViewById(R.id.myrv);
        categoryAdapter = new ExamAdapter(this, questionArrayList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        //recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(categoryAdapter);

    }


}