package com.loksewaquiz;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loksewaquiz.Model_classes.QuestionsResponse;
import com.loksewaquiz.app.AppController;
import com.loksewaquiz.dto.PackageDTO;

import java.lang.ref.WeakReference;
import java.lang.reflect.Type;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by kumar on 7/2/2017.
 */

public class Fetch_Package_Task  extends AsyncTask<Void,Void,Void> {

    WeakReference<Context> mContext;

    public Fetch_Package_Task(Context context)
    {
        mContext = new WeakReference<Context>(context);
    }

    @Override
    protected Void doInBackground(Void... params) {
        OkHttpClient client = new OkHttpClient();
        try{
            Request request = new Request.Builder()
                    .url("http://loksewaexam.loksewaguide.com/loksewaquiz/api/user/packageFetch.php")
                .build();

            Response response = client.newCall(request).execute();
            String jsonResponse = response.body().string();
            Log.e("JSON RESPONSE", jsonResponse);

            Gson gson = new Gson();

            Type listType = new TypeToken<List<PackageDTO>>(){}.getType();
            List<PackageDTO> packageList = gson.fromJson(jsonResponse, listType);

            AppController.getInstance().setPackageList(packageList);

        }catch (Exception e){

        }



        return null;
    }
}
