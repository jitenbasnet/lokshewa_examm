package com.loksewaquiz;

import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.loksewaquiz.Model_classes.Question;
import com.loksewaquiz.Model_classes.QuestionsResponse;
import com.loksewaquiz.app.AppController;
import com.squareup.picasso.Picasso;


import java.util.ArrayList;
import java.util.List;

import static com.loksewaquiz.R.id.imageView;

public class ResultDetails extends AppCompatActivity {

    List<Question> questions = new ArrayList<>();
    int correct, unanswered, incorrect;
    private TableLayout tableLayout;
    private TextView blankText;
    private TextView question_correct_text;
    private TextView correct_answer;
    private TextView question_unanswered_text;
    private ImageView ans_image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.table);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        AdView adView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .setRequestAgent("android_studio:ad_template").build();
        adView.loadAd(adRequest);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

            getSupportActionBar().setTitle("LokSewa Exam Result");
        }


        LinearLayout linearlayout = (LinearLayout) findViewById(R.id.linear);


        tableLayout = (TableLayout) findViewById(R.id.tableLayout);

        questions = AppController.getInstance().getQuestionList();


        correct = incorrect = unanswered = 0;

        for (Question q : questions) {
            if (q.isAnswered()) {
                if (q.isCorrect()) {
                    correct++;
                    // Toast.makeText(this, "Correct Answer" + q.getQuestionsResponse().getTitle() + q.getSelectedAnswer(), Toast.LENGTH_SHORT).show();
                } else {
                    // Toast.makeText(this, "InCorrect Answer" + q.getCorrect().getOptionOption() + "Your selection" + q.getSelectedAnswer(), Toast.LENGTH_SHORT).show();
                    incorrect++;


                }
            } else {
                unanswered++;
                //Toast.makeText(this, "Unanswered" + q.getQuestionsResponse().getTitle() + " " + q.getCorrect().getOptionOption() , Toast.LENGTH_SHORT).show();
            }
        }
        for (int i = 0; i < 1; i++) {
            View tableRow = LayoutInflater.from(this).inflate(R.layout.table_item, null, false);
            TextView correct_view = (TextView) tableRow.findViewById(R.id.correct_id);
            TextView incorrect_view = (TextView) tableRow.findViewById(R.id.incorrect_id);
            TextView history_display_orderid = (TextView) tableRow.findViewById(R.id.unanswered_id);
            correct_view.setText(String.valueOf(correct));
            incorrect_view.setText(String.valueOf(incorrect));
            history_display_orderid.setText(String.valueOf(unanswered));
            tableLayout.addView(tableRow);

        }
        for (int i = 0; i < questions.size(); i++) {
            blankText = new TextView(this);
            question_correct_text = new TextView(this);
            ans_image= new ImageView(this);
            correct_answer = new TextView(this);
            question_unanswered_text = new TextView(this);
            //customize views
            Typeface boldTypeface = Typeface.defaultFromStyle(Typeface.BOLD);
            question_correct_text.setTypeface(boldTypeface);
            question_correct_text.setBackgroundColor(Color.parseColor("#006064"));
            question_correct_text.setTextColor(Color.parseColor("#ffffff"));
            question_correct_text.setTextSize(15);
            correct_answer.setBackgroundColor(Color.parseColor("#43A047"));
            question_unanswered_text.setBackgroundColor(Color.parseColor("#ECEFF1"));
            correct_answer.setTextSize(14);
            correct_answer.setTextColor(Color.parseColor("#ffffff"));
            question_unanswered_text.setTextColor(Color.parseColor("#D84315"));
            ans_image.requestLayout();
            question_unanswered_text.setTextSize(14);
            blankText.setText(" ");
            question_correct_text.setText((1+i+">") + " " + questions.get(i).getQuestionsResponse().getTitle());
            if(questions.get(i).getQuestionsResponse().hasImage()){
                String url = questions.get(i).getQuestionsResponse().getImage();
                Picasso.with(this).load("http://www.loksewaguide.com" + url).into(ans_image);
                correct_answer.setText("Correct Ans:" + " " + questions.get(i).getCorrect().getOptionOption());
                question_unanswered_text.setText("Your Option:" + " " + questions.get(i).getSelectedAnswer());
            }
            correct_answer.setText("Correct Ans:" + " " + questions.get(i).getCorrect().getOptionOption());
            question_unanswered_text.setText("Your Option:" + " " + questions.get(i).getSelectedAnswer());
                linearlayout.addView(question_correct_text);
            linearlayout.addView(ans_image);
                linearlayout.addView(correct_answer);
                linearlayout.addView(question_unanswered_text);
                linearlayout.addView(blankText);


            }




        }



    }


    //Toast.makeText(this, questions.get(2).getQuestionsResponse().getTitle() + questions.get(2).getSelectedAnswer() + "   "   + questions.get(2).getCorrect().getOptionOption() + correct + " " + incorrect +  " " + unanswered, Toast.LENGTH_SHORT).show();

