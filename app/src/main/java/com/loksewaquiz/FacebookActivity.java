package com.loksewaquiz;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.loksewaquiz.activity.FacebookRegisteration;
import com.loksewaquiz.helper.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;


public class FacebookActivity extends AppCompatActivity {
    Context c;
    SessionManager session;
    private CallbackManager mCallbackManager;
    public static final String PROFILE_NAME = "PROFILE_FIRST_NAME";
    public static final String PROFILE_EMAIL = "PROFILE_EMAIL";
    public static final String PROFILE_IMAGE_URL = "PROFILE_IMAGE_URL";
    public static final String PROFILE_ID = "PROFILE_ID";
    public static final String PROFILE_FIRST_NAME = "PROFILE_FIRST_NAME";
    public static final String PROFILE_LAST_NAME = "PROFILE_LAST_NAME";
    TextView title_tv, sub_title_tv;
    String imageName = "alert";
    String email, fullName, last_name, first_name;
    String type = "login";
    boolean isStart;
    String id;
    FacebookRegisteration  fbregister;
    String checkActivity = "check";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_facebook);
        AdView adView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .setRequestAgent("android_studio:ad_template").build();
        adView.loadAd(adRequest);
        final LoginButton mLoginButton = (LoginButton) findViewById(R.id.login_button);
        session = new SessionManager(getApplicationContext());
        mCallbackManager = CallbackManager.Factory.create();
        fbregister=new FacebookRegisteration();
        mLoginButton.setReadPermissions("public_profile, email");
        if (AccessToken.getCurrentAccessToken() != null) {
            RequestData();
            isStart = true;

            //AccessToken.setCurrentAccessToken(null);
            mLoginButton.setVisibility(View.INVISIBLE);
        }

        mLoginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.e("LOGIN_SUCCESS", "Success");
                session.setFBLogin(true);


                // String userLoginId = loginResult.getAccessToken().getUserId();
                mLoginButton.setVisibility(View.INVISIBLE); //<- IMPORTANT

                if (AccessToken.getCurrentAccessToken() != null) {
                    RequestData();
                    isStart = true;
                    //AccessToken.setCurrentAccessToken(null);
                }


            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException error) {
                Log.e("ERROR",error.getMessage());
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data
        );
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //AppEventsLogger.activateApp(this); is deprecated
        AppEventsLogger.activateApp(getApplication());
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    //Method to request data from facebook Graph API
    public void RequestData() {
        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                final Intent facebookIntent = new Intent(FacebookActivity.this, NavigationActivity.class);

                JSONObject json = response.getJSONObject();

                try {
                    if (json != null) {

                        if (json.has("email") && json.has("name")) {
                            email = json.getString("email");
                            first_name = json.optString("first_name");
                            last_name = json.optString("last_name");
                            fullName = last_name;
                            id = json.optString("id");
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                facebookIntent.putExtra(PROFILE_EMAIL, (Html.fromHtml(email, 0)));
                                facebookIntent.putExtra(PROFILE_NAME, fullName);
                                facebookIntent.putExtra(PROFILE_ID, id);
                                facebookIntent.putExtra(PROFILE_FIRST_NAME, first_name);
                                facebookIntent.putExtra(PROFILE_LAST_NAME, last_name);

                            } else {
                                facebookIntent.putExtra(PROFILE_EMAIL, (Html.fromHtml(email)));
                                facebookIntent.putExtra(PROFILE_NAME, fullName);
                                facebookIntent.putExtra(PROFILE_ID, id);
                                facebookIntent.putExtra(PROFILE_FIRST_NAME, first_name);
                                facebookIntent.putExtra(PROFILE_LAST_NAME, last_name);
                            }
                            if (isStart) {
                                isStart = false;

                                // AccessToken.setCurrentAccessToken(null);
                                //saves user name and email

                                fbregister.registerUser(first_name,last_name,email,fullName,"",id);
                                Log.e("FbRegister","called");
                                Log.e("CHECKVALUE",first_name+last_name +email +fullName +id);
                            } else {
                                Toast.makeText(FacebookActivity.this, "background work not called", Toast.LENGTH_LONG).show();
                            }


                        } else {

                            email = "";
                            id = json.optString("id");
                            first_name = json.optString("first_name");
                            last_name = json.optString("last_name");
                            fullName = last_name;

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                facebookIntent.putExtra(PROFILE_EMAIL, (Html.fromHtml(email, 0)).toString());
                                facebookIntent.putExtra(PROFILE_NAME, fullName);
                                facebookIntent.putExtra(PROFILE_ID, id);
                                facebookIntent.putExtra(PROFILE_FIRST_NAME, first_name);
                                facebookIntent.putExtra(PROFILE_LAST_NAME, last_name);
                            } else {
                                facebookIntent.putExtra(PROFILE_EMAIL, (Html.fromHtml(email).toString()));
                                facebookIntent.putExtra(PROFILE_NAME, fullName);
                                facebookIntent.putExtra(PROFILE_ID, id);
                                facebookIntent.putExtra(PROFILE_FIRST_NAME, first_name);
                                facebookIntent.putExtra(PROFILE_LAST_NAME, last_name);
                            }
                            if (isStart) {
                                isStart = false;
                                // AccessToken.setCurrentAccessToken(null);
                                //saves user name and email
                                fbregister.registerUser(first_name,last_name,email,fullName,"",id);

                            } else {
                                Toast.makeText(FacebookActivity.this, "background work not called", Toast.LENGTH_LONG).show();
                            }


                        }
                        String profile_picture = json.getString("id");
                        facebookIntent.putExtra(PROFILE_IMAGE_URL, profile_picture);
                        startActivity(facebookIntent);
                        finish();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,email,name,picture,first_name,last_name");
        request.setParameters(parameters);
        request.executeAsync();
        session.createFBLoginSession(fullName,email);
        session.createManualLoginSession(fullName,email);
        session.setFBLogin(true);
        session.setManualLogin(false);

    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(FacebookActivity.this,NavigationActivity.class);
        startActivity(i);
        finish();

    }


}
