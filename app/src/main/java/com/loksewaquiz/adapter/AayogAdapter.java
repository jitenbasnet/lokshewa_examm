package com.loksewaquiz.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.loksewaquiz.R;
import com.loksewaquiz.LoksewaAayog;
import com.loksewaquiz.util.PrefUtil;

import java.util.List;

/**
 * Created by kumar on 6/15/2017.
 */

public class AayogAdapter extends RecyclerView.Adapter<AayogAdapter.MyViewHolder>{

    private List<String> stringList;
    private Context context;
    private int row_index;
    public static class MyViewHolder extends RecyclerView.ViewHolder
    {
        public TextView textView;
        public MyViewHolder(View view)
        {
            super(view);

            textView = (TextView)view.findViewById(R.id.text1);
        }
    }

    public AayogAdapter(List<String> aayogList, Context context)
    {
        this.stringList = aayogList;
        this.context = context;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_loksewa_aayog,parent,false);
        return new  MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item,parent,false));
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final String string = stringList.get(position);
        holder.textView.setText(string);
        Log.e("o", string);
        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(PrefUtil.getString("inMainPage","Yes", context).equals("Yes")){
                    //Toast.makeText(context, "Here", Toast.LENGTH_SHORT).show();

                ((LoksewaAayog)context).setUpSecondaryPage(stringList.get(position));

                }else{
                    //Toast.makeText(context, "THere", Toast.LENGTH_SHORT).show();

                    ((LoksewaAayog)context).goDownload(position);
                }

            }
        });


    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }


}
