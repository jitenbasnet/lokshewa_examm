package com.loksewaquiz.adapter;

import android.content.Context;
import android.content.Intent;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.loksewaquiz.MainActivity;
import com.loksewaquiz.R;
import com.loksewaquiz.dto.PackageDTO;
import com.loksewaquiz.helper.SessionManager;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by kumar on 7/2/2017.
 */

public class PackageAdapter extends RecyclerView.Adapter<PackageAdapter.PackageViewHolder> {
    private SessionManager session;
    private Context context;
    private List<PackageDTO> packageDTOList;
    public PackageAdapter(Context context, List<PackageDTO> packageList){
        this.context = context;
        this.packageDTOList = packageList;
    }

    public class PackageViewHolder extends RecyclerView.ViewHolder{

        public TextView textView;

        public ImageView imageView;

        public PackageViewHolder(View view)
        {
            super(view);
            textView = (TextView) view.findViewById(R.id.packageName);
            imageView = (ImageView)view.findViewById(R.id.packageImage);
        }
    }

    @Override
    public PackageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_loksewa_aayog,parent,false);
        return new PackageViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.package_row,parent,false));

    }

    @Override
    public void onBindViewHolder(PackageViewHolder holder, final int position) {

        PackageDTO packageDTO = packageDTOList.get(position);

        holder.textView.setText(packageDTO.getPackage_name());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                session = new SessionManager(v.getContext());
                Log.e("FBLogin",String.valueOf(session.isFbLoggedIn()));
                Log.e("Manual Login",String.valueOf(session.isManualLoggedIn()));
                Log.e("Logged IN",String.valueOf(session.isLoggedIn()));
                if(session.isFbLoggedIn() || session.isManualLoggedIn() || session.isLoggedIn()) {
                    Intent intent = new Intent(context, MainActivity.class);
                    intent.putExtra("position", position);
                    context.startActivity(intent);
                }else{
                    session.checkLogin();
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return packageDTOList.size();
    }
}
