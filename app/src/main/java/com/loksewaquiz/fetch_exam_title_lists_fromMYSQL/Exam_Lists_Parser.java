package com.loksewaquiz.fetch_exam_title_lists_fromMYSQL;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.loksewaquiz.Model_classes.Exam_Title_Lists;
import com.loksewaquiz.NavigationActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;

/**
 * Created by Digition on 6/8/2017.
 */

public class Exam_Lists_Parser extends AsyncTask<Void, Void, Boolean> {


    Context c;
    String jsonData;
    ProgressDialog pd;
    Exam_Title_Lists ls;
    ArrayList<Exam_Title_Lists> exam_title_list = new ArrayList<>();

    public Exam_Lists_Parser(Context c, String jsonData) {
        this.c = c;
        this.jsonData = jsonData;

    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();

            pd = new ProgressDialog(c);
            pd.setTitle("Downloading..");
            pd.setMessage("Fetching your jobs..");
            pd.show();




    }

    @Override
    protected Boolean doInBackground(Void... params) {

        return this.parseData();
    }

    @Override
    protected void onPostExecute(Boolean parsed) {
        super.onPostExecute(parsed);
        if(pd.isShowing()){
            pd.dismiss();
        }




        if (parsed) {
            //BIND data
            NavigationActivity screen= new NavigationActivity();
            screen.NavigationActivitys(c,exam_title_list);
            ls=exam_title_list.get(3);
            String s=ls.getPackage_name();
            Log.d("QuestionParser",s);





        } else {
            Toast.makeText(c, "Unable to Parse", Toast.LENGTH_SHORT).show();

        }

    }

    private Boolean parseData() {
        try {

            JSONArray ja = new JSONArray(jsonData);
            JSONObject jo;
            exam_title_list.clear();
            Exam_Title_Lists exam_tiltle_lists;
            for (int i = 0; i < ja.length(); i++) {
                //GETTING DATA FROM DATABASE
                jo = ja.getJSONObject(i);
                int id = jo.getInt("id");
                String package_name = jo.getString("package_name");
                int time = jo.getInt("time");
                int full_mark = jo.getInt("full_mark");
                int pass_mark = jo.getInt("pass_mark");
                int number_of_question = jo.getInt("number_of_question");
                int mark_of_each_question = jo.getInt("mark_of_each_question");
                //Setting obtained value to respective variables
                exam_tiltle_lists = new Exam_Title_Lists();
                exam_tiltle_lists.setId(id);
                exam_tiltle_lists.setPackage_name(package_name);
                exam_tiltle_lists.setTime(time);
                exam_tiltle_lists.setFull_mark(full_mark);
                exam_tiltle_lists.setPass_mark(pass_mark);
                exam_tiltle_lists.setNumber_of_question(number_of_question);
                exam_tiltle_lists.setMark_of_each_question(mark_of_each_question);

                exam_title_list.add(exam_tiltle_lists);

            }
            return true;
        } catch (JSONException e) {
            System.out.println("Json Exception");
        }
        return false;
    }
}