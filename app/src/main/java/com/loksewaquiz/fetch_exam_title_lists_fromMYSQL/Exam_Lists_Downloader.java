package com.loksewaquiz.fetch_exam_title_lists_fromMYSQL;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;

import com.loksewaquiz.NavigationActivity;
import com.loksewaquiz.R;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

/**
 * Created by Digition on 6/8/2017.
 */

public class Exam_Lists_Downloader extends AsyncTask<Void, Void, String> {

    Context c;
    String urlAddress;
    ProgressDialog pd;


    public Exam_Lists_Downloader(Context c, String urlAddress) {
        this.c = c;
        this.urlAddress = urlAddress;

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        pd = new ProgressDialog(c);
        pd.setTitle("EXAM TITLE");
        pd.setMessage("Retrieving...please wait");
    }

    @Override
    protected String doInBackground(Void... params) {
        return this.downloadData();
    }

    @Override
    protected void onPostExecute(String jsonData) {
        super.onPostExecute(jsonData);
        pd.dismiss();
        if (jsonData == null) {
                showAlertDialog(c, "OOPS!", "No internet connection", R.drawable.alert);


        } else {
            new Exam_Lists_Parser(c, jsonData).execute();

        }


    }

    private String downloadData() {
        HttpURLConnection con = Exam_Lists_Connector.connect(urlAddress);
        if (con == null) {
            return null;
        }
        try {

            InputStream is = new BufferedInputStream(con.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line;

            StringBuilder jsonData = new StringBuilder();

            while ((line = br.readLine()) != null) {

                jsonData.append(line + "\n");
            }
            br.close();
            is.close();
            return jsonData.toString();

        } catch (IOException e) {
            e.printStackTrace();

        }
        return null;
    }

    //alert dialog box
    private void showAlertDialog(Context mContext, String mTitle, String mBody, int mImage) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setCancelable(true);
        builder.setIcon(mImage);
        if (mTitle.length() > 0)
            builder.setTitle(mTitle);
        if (mBody.length() > 0)
            builder.setTitle(mBody);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.create().show();
    }


}
