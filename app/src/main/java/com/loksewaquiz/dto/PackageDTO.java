package com.loksewaquiz.dto;

/**
 * Created by kumar on 7/2/2017.
 */

public class PackageDTO {

    private int id;

    private String package_name;

    private int time;

    private int full_mark;

    private int pass_mark;

    private int number_of_questions;

    private int marks_of_each_question;


    public int getId() {
        return id;
    }

    public int getPass_mark()
    {
        return pass_mark;
    }

    public String getPackage_name() {
        return package_name;
    }

    public int getTime() {
        return time;
    }

    public int getFull_mark() {
        return full_mark;
    }

    public int getNumber_of_questions() {
        return number_of_questions;
    }

    public int getMarks_of_each_question() {
        return marks_of_each_question;
    }
}
