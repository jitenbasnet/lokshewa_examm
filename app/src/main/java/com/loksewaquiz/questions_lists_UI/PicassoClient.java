package com.loksewaquiz.questions_lists_UI;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import com.squareup.picasso.Picasso;

/**
 * Created by Digition on 6/14/2016.
 */
public class PicassoClient {

    public static void downloadImage(Context c, String imageUrl, ImageView img) {
        if (imageUrl!= null &&imageUrl.length() > 0) {
            Picasso.with(c).load((imageUrl)).into(img);


        }
        else{
            img.setVisibility(View.GONE);
        }

    }
}
