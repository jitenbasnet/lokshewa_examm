package com.loksewaquiz.questions_lists_UI;

import android.content.Context;
import android.support.annotation.IdRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.view.ViewGroup.LayoutParams;
import android.widget.Toast;

import com.loksewaquiz.Model_classes.Question;
import com.loksewaquiz.Model_classes.AnsOption;
import com.loksewaquiz.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by kumar on 6/21/2017.
 */

public class ExamAdapter extends RecyclerView.Adapter<ExamAdapter.CustomViewHolder> {

    private List<Question> questionList;
    private Context context;
    String space="";

    public ExamAdapter(Context context, List<Question> questionList) {
        this.questionList = questionList;
        this.context = context;
    }


    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.from(parent.getContext()).inflate(R.layout.questions_view_model, parent, false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder holder, final int position) {

        holder.qtextView.setText(questionList.get(position).getQuestionsResponse().getTitle());
        if (questionList.get(position).getQuestionsResponse().hasImage()) {
         String url="http://www.loksewaguide.com"+(questionList.get(position).getQuestionsResponse().getImage());
                Picasso.with(context).load(url).into(holder.imageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        holder.progressbar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {

                    }
                });

         }
         else{
            holder.imageView.setImageDrawable(null);
        }
        
        holder.radioGroup.setTag(position);
        List<AnsOption> ansOptions = questionList.get(position).getQuestionsResponse().ansOption;
        int count = (int) Math.random();

        holder.radioGroup.removeAllViews();
        for (AnsOption option : ansOptions) {
            RadioButton radioButton = new RadioButton(context);
            radioButton.setText(space+option.getOptionOption());
            radioButton.setId(count++);
            radioButton.setText(space+option.getOptionOption());
            radioButton.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
            //radioButton.setBackgroundColor(Color.parseColor("#ffffff"));
            radioButton.setButtonDrawable(R.drawable.custom_radio_button);
            //setting color of the devider for radio buttons
            View v = new View(context);
            v.setLayoutParams(new RadioGroup.LayoutParams(LayoutParams.MATCH_PARENT, 1));
            v.setBackgroundColor(ContextCompat.getColor(context, R.color.Black));
            holder.radioGroup.addView(v);

            //Toast.makeText(context, option.getOptionOption(), Toast.LENGTH_SHORT).show();
            holder.radioGroup.addView(radioButton);


        }

        holder.radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                //Toast.makeText(context, checkedId + " ", Toast.LENGTH_SHORT).show();

                int id = group.getCheckedRadioButtonId();


                RadioButton radioButton = (RadioButton) group.findViewById(id);
                //Toast.makeText(context, radioButton.getText(), Toast.LENGTH_SHORT).show();
                int clickedPos = (Integer) group.getTag();

                if(radioButton != null) {
                    String customTag = radioButton.getText().toString();
                    questionList.get(clickedPos).setSelectedAnswer(customTag);
                }

            }
        });
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {

        public TextView qtextView;
        public RadioGroup radioGroup;
        public ImageView imageView;
        public ProgressBar progressbar;

        public CustomViewHolder(View view) {
            super(view);
            this.qtextView = (TextView) view.findViewById(R.id.question);
            this.radioGroup = (RadioGroup) view.findViewById(R.id.radioGroup);
            this.imageView = (ImageView) view.findViewById(R.id.option_image);
            this.progressbar=(ProgressBar) view.findViewById(R.id.progress);

        }

    }


    @Override
    public int getItemCount() {
        return questionList.size();
    }
}
